package rsa

import (
	cryptoRsa "crypto/rsa"
	"io/ioutil"

	jwt "github.com/dgrijalva/jwt-go"
)

type Rsa struct{}

func NewRsa() *Rsa {
	return &Rsa{}
}

func (Rsa) ParsePrivateKey(filePath string) (*cryptoRsa.PrivateKey, error) {
	keyBytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	key, err := jwt.ParseRSAPrivateKeyFromPEM(keyBytes)
	if err != nil {
		return nil, err
	}

	return key, nil
}

func (Rsa) ParsePublicKey(filePath string) (*cryptoRsa.PublicKey, error) {
	keyBytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	key, err := jwt.ParseRSAPublicKeyFromPEM(keyBytes)
	if err != nil {
		return nil, err
	}

	return key, nil
}
