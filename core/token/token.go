package token

import (
	cryptoRsa "crypto/rsa"
	"errors"
	"net/http"
	"time"

	"bitbucket.org/strom87/api-nearest-injury/config"
	"bitbucket.org/strom87/api-nearest-injury/core/factory"
	"bitbucket.org/strom87/api-nearest-injury/core/helper"
	"bitbucket.org/strom87/api-nearest-injury/core/rsa"
	"bitbucket.org/strom87/db-nearest-injury"
	"github.com/dgrijalva/jwt-go"
	"gopkg.in/mgo.v2/bson"
)

const (
	Web = "web"
	App = "app"
)

type Token struct {
	Rsa    *rsa.Rsa
	Config *config.Config
}

type TokenData struct {
	Token  string
	Secret string
}

type TokenClaim struct {
	Id       string
	Platform string
}

func NewToken() *Token {
	return &Token{Rsa: rsa.NewRsa(), Config: config.NewConfig()}
}

// GetTokens platform type "web" or "app"
func (t Token) GetTokens(user db.User, platform string) (*TokenData, *TokenData, error) {
	authToken, err := t.GetAuthToken(user, platform)
	if err != nil {
		return nil, nil, err
	}

	refreshToken, err := t.GetRefreshToken(user, platform)
	if err != nil {
		return nil, nil, err
	}

	return authToken, refreshToken, nil
}

// GetAuthToken platform type "web" or "app"
func (t Token) GetAuthToken(user db.User, platform string) (*TokenData, error) {
	if !t.IsValidPlatform(platform) {
		return nil, errors.New("Invalid platform")
	}

	privateKey, err := t.Rsa.ParsePrivateKey(t.Config.Rsa.PrivateFilePath)
	if err != nil {
		return nil, err
	}

	secret := helper.NewHelper().RandomString(16)
	tokenString, err := t.generateAuthToken(user, *privateKey, platform, secret)
	if err != nil {
		return nil, err
	}

	return &TokenData{Token: tokenString, Secret: secret}, nil
}

// GetRefreshToken platform type "web" or "app"
func (t Token) GetRefreshToken(user db.User, platform string) (*TokenData, error) {
	if !t.IsValidPlatform(platform) {
		return nil, errors.New("Invalid platform")
	}

	privateKey, err := t.Rsa.ParsePrivateKey(t.Config.Rsa.PrivateFilePath)
	if err != nil {
		return nil, err
	}

	secret := helper.NewHelper().RandomString(16)
	tokenString, err := t.generateRefreshToken(user, *privateKey, platform, secret)
	if err != nil {
		return nil, err
	}

	return &TokenData{Token: tokenString, Secret: secret}, nil
}

func (t Token) ValidateAuthToken(req *http.Request, conn *db.Connection) (bool, TokenClaim, error) {
	publicKey, err := t.Rsa.ParsePublicKey(config.NewConfig().Rsa.PublicFilePath)
	if err != nil {
		return false, TokenClaim{}, err
	}

	token, err := jwt.ParseFromRequest(req, func(token *jwt.Token) (interface{}, error) {
		if token == nil {
			return nil, errors.New("Auth token required")
		}
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, errors.New("Unexpected signing method: " + token.Header["alg"].(string))
		}
		if token.Claims["usage"].(string) != "auth" {
			return nil, errors.New("Not a valid auth token")
		}

		if err := t.hasExpired(token.Claims["exp"].(string)); err != nil {
			return nil, err
		}

		user, err := t.getUser(conn, token.Claims["id"].(string))
		if err != nil {
			return nil, err
		}

		secret := token.Claims["secret"].(string)
		platform := token.Claims["platform"].(string)
		if err := t.isValidAuthSecret(*user, secret, platform); err != nil {
			return nil, err
		}

		return publicKey, nil
	})

	if err != nil {
		return false, TokenClaim{}, err
	}

	tokenClaim := TokenClaim{
		Id:       token.Claims["id"].(string),
		Platform: token.Claims["platform"].(string),
	}

	return token.Valid, tokenClaim, nil
}

func (t Token) ValidateAuthTokenString(authToken string, conn *db.Connection) (bool, TokenClaim, error) {
	publicKey, err := t.Rsa.ParsePublicKey(config.NewConfig().Rsa.PublicFilePath)
	if err != nil {
		return false, TokenClaim{}, err
	}

	token, err := jwt.Parse(authToken, func(token *jwt.Token) (interface{}, error) {
		if token == nil {
			return nil, errors.New("Auth token required")
		}
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, errors.New("Unexpected signing method: " + token.Header["alg"].(string))
		}
		if token.Claims["usage"].(string) != "auth" {
			return nil, errors.New("Not a valid auth token")
		}

		if err := t.hasExpired(token.Claims["exp"].(string)); err != nil {
			return nil, err
		}

		user, err := t.getUser(conn, token.Claims["id"].(string))
		if err != nil {
			return nil, err
		}

		secret := token.Claims["secret"].(string)
		platform := token.Claims["platform"].(string)
		if err := t.isValidAuthSecret(*user, secret, platform); err != nil {
			return nil, err
		}

		return publicKey, nil
	})

	if err != nil {
		return false, TokenClaim{}, err
	}

	tokenClaim := TokenClaim{
		Id:       token.Claims["id"].(string),
		Platform: token.Claims["platform"].(string),
	}

	return token.Valid, tokenClaim, nil
}

func (t Token) ValidateRefreshToken(refreshToken string, conn *db.Connection) (bool, TokenClaim, error) {
	publicKey, err := t.Rsa.ParsePublicKey(config.NewConfig().Rsa.PublicFilePath)
	if err != nil {
		return false, TokenClaim{}, err
	}

	token, err := jwt.Parse(refreshToken, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, errors.New("Unexpected signing method: " + token.Header["alg"].(string))
		}
		if token.Claims["usage"].(string) != "refresh" {
			return nil, errors.New("Not a valid refresh token")
		}

		if err := t.hasExpired(token.Claims["exp"].(string)); err != nil {
			return nil, err
		}

		user, err := t.getUser(conn, token.Claims["id"].(string))
		if err != nil {
			return nil, err
		}

		secret := token.Claims["secret"].(string)
		platform := token.Claims["platform"].(string)
		if err := t.isValidRefreshSecret(*user, secret, platform); err != nil {
			return nil, err
		}

		return publicKey, nil
	})

	if err != nil {
		return false, TokenClaim{}, err
	}

	tokenClaim := TokenClaim{
		Id:       token.Claims["id"].(string),
		Platform: token.Claims["platform"].(string),
	}

	return token.Valid, tokenClaim, nil
}

func (Token) IsValidPlatform(platform string) bool {
	return platform == Web || platform == App
}

func (Token) SetWebTokenSecret(conn *db.Connection, user db.User, authSecret, refreshSecret string) error {
	provider := factory.NewDatabase().User(conn)

	if err := provider.SetAuthTokenWebSecret(user, authSecret); err != nil {
		return err
	}

	if err := provider.SetRefreshTokenWebSecret(user, refreshSecret); err != nil {
		return err
	}

	return nil
}

func (Token) SetAppTokenSecret(conn *db.Connection, user db.User, authSecret, refreshSecret string) error {
	provider := factory.NewDatabase().User(conn)

	if err := provider.SetAuthTokenAppSecret(user, authSecret); err != nil {
		return err
	}

	if err := provider.SetRefreshTokenAppSecret(user, refreshSecret); err != nil {
		return err
	}

	return nil
}

func (t Token) generateAuthToken(user db.User, privateKey cryptoRsa.PrivateKey, platform, secret string) (string, error) {
	token := jwt.New(jwt.SigningMethodRS256)
	token.Claims["id"] = user.Id
	token.Claims["exp"] = time.Now().Add(time.Minute * time.Duration(t.Config.Token.AuthExpireTime))
	token.Claims["iat"] = time.Now()
	token.Claims["usage"] = "auth"
	token.Claims["secret"] = secret
	token.Claims["platform"] = platform

	tokenString, err := token.SignedString(&privateKey)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func (Token) generateRefreshToken(user db.User, privateKey cryptoRsa.PrivateKey, platform, secret string) (string, error) {
	token := jwt.New(jwt.SigningMethodRS256)
	token.Claims["id"] = user.Id
	token.Claims["exp"] = time.Now().Add(((time.Hour * 24) * 356) * 5)
	token.Claims["iat"] = time.Now()
	token.Claims["usage"] = "refresh"
	token.Claims["secret"] = secret
	token.Claims["platform"] = platform

	tokenString, err := token.SignedString(&privateKey)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func (Token) getUser(conn *db.Connection, id string) (*db.User, error) {
	if !bson.IsObjectIdHex(id) {
		return nil, errors.New("Not a valid user id")
	}

	user, err := factory.NewDatabase().User(conn).Find(bson.ObjectIdHex(id))
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (t Token) isValidAuthSecret(user db.User, secret, platform string) error {
	if !t.IsValidPlatform(platform) {
		return errors.New("Not a valid platform")
	}

	switch platform {
	case Web:
		if user.TokenSecret.Web.Auth != secret {
			return errors.New("Invalid web secret key")
		}
	case App:
		if user.TokenSecret.App.Auth != secret {
			return errors.New("Invalid app secret key")
		}
	}

	return nil
}

func (t Token) isValidRefreshSecret(user db.User, secret, platform string) error {
	if !t.IsValidPlatform(platform) {
		return errors.New("Not a valid platform")
	}

	switch platform {
	case Web:
		if user.TokenSecret.Web.Refresh != secret {
			return errors.New("Invalid web secret key")
		}
	case App:
		if user.TokenSecret.App.Refresh != secret {
			return errors.New("Invalid app secret key")
		}
	}

	return nil
}

func (Token) hasExpired(expTime string) error {
	expires, err := time.Parse(time.RFC3339, expTime)
	if err != nil {
		return err
	}
	if time.Now().After(expires) {
		return errors.New("Token expired")
	}

	return nil
}
