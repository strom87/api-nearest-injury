package logger

import "log"

type Logger struct{}

func NewLogger() *Logger {
	return &Logger{}
}

func (Logger) Log(message string, err error) {
	if err != nil {
		log.Println(message, err)
	}
}
