package helper

import (
	"math/rand"
	"time"

	"gopkg.in/mgo.v2/bson"
)

type Helper struct{}

func NewHelper() *Helper {
	return &Helper{}
}

func (Helper) ContainsBsonId(id bson.ObjectId, ids []bson.ObjectId) bool {
	for _, k := range ids {
		if k == id {
			return true
		}
	}

	return false
}

func (Helper) IsStringObjectId(id string) bool {
	return bson.IsObjectIdHex(id)
}

func (Helper) RandomString(length int32) string {
	rand.Seed(time.Now().UnixNano())
	runeString := []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

	str := make([]rune, length)
	for i := range str {
		str[i] = runeString[rand.Intn(len(runeString))]
	}
	return string(str)
}

func (Helper) UserImageUrl(size, fileName string) string {
	return "/image/user/" + size + "/" + fileName
}

func (Helper) SpotImageUrl(size, fileName string) string {
	return "/image/spot/" + size + "/" + fileName
}
