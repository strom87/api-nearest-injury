package encryption

import (
	"log"
	"strings"
	"time"

	"bitbucket.org/strom87/api-nearest-injury/core/helper"
	"golang.org/x/crypto/bcrypt"
)

const (
	SaltLength  = 24
	EncryptCost = 12
	RehashDays  = 14
)

// Handles create a new hash/salt combo from a raw password as inputted by the user
func MakePassword(rawPassword string) (string, string) {
	salt := generateSalt()
	saltedPass := combine(salt, rawPassword)
	password := hashPassword(saltedPass)

	return password, salt
}

// Checks whether or not the correct password has been provided
func MatchPasswords(rawPassword string, hashedPassword string, salt string) bool {
	saltedGuess := combine(salt, rawPassword)

	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(saltedGuess)) == nil
}

// Creates the date that the password should be rehased at
func GetNewPasswordRehashDate() time.Time {
	return time.Now().AddDate(0, 0, RehashDays)
}

// Checks if the date is passed and the password needs to be rehased
func PasswordRehashNeeded(rehashDate time.Time) bool {
	return time.Now().After(rehashDate)
}

// this handles taking a raw user password and making in into something safe for storing in our DB
func hashPassword(saltedPassword string) string {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(saltedPassword), EncryptCost)
	if err != nil {
		log.Fatal(err)
	}

	return string(hashedPassword)
}

// Handles merging together the salt and the password
func combine(salt string, rawPassword string) string {
	pieces := []string{salt, rawPassword}
	saltedPassword := strings.Join(pieces, "")
	return saltedPassword
}

// Generates a random salt using DevNull
func generateSalt() string {
	return helper.NewHelper().RandomString(SaltLength)
}
