package encryption

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"io"
)

const (
	key = "xMn29s7rfnSJALmso26Af!3q?3vaCXi5"
)

func EncryptAes(text string) (string, error) {
	return encrypt([]byte(key), text)
}

func DecryptAes(text string) (string, error) {
	return decrypt([]byte(key), text)
}

// encrypt string to base64 crypto using AES
func encrypt(key []byte, text string) (string, error) {
	// key := []byte(keyText)
	plainText := []byte(text)

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	cipherText := make([]byte, aes.BlockSize+len(plainText))
	iv := cipherText[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return "", err
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(cipherText[aes.BlockSize:], plainText)

	// convert to base64
	return base64.URLEncoding.EncodeToString(cipherText), nil
}

// decrypt from base64 to decrypted string
func decrypt(key []byte, ctyptoText string) (string, error) {
	cipherText, _ := base64.URLEncoding.DecodeString(ctyptoText)

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	if len(cipherText) < aes.BlockSize {
		return "", errors.New("Cipher text too short")
	}
	iv := cipherText[:aes.BlockSize]
	cipherText = cipherText[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)

	// XORKeyStream can work in-place if the two arguments are the same.
	stream.XORKeyStream(cipherText, cipherText)

	return string(cipherText), nil
}
