package factory

import (
	"bitbucket.org/strom87/api-nearest-injury/config"
	"bitbucket.org/strom87/db-nearest-injury"
)

type Database struct {
	config *config.Config
}

func NewDatabase() *Database {
	return &Database{config: config.NewConfig()}
}

func (d Database) Connection() *db.Connection {
	return db.NewConnection(d.config.Database.Name, d.config.Database.Connection)
}

func (d Database) User(connection *db.Connection) *db.UserProvider {
	return db.NewUserProvider(connection)
}

func (d Database) UserImage(connection *db.Connection) *db.UserImageProvider {
	return db.NewUserImageProvider(connection)
}

func (d Database) Spot(connection *db.Connection) *db.SpotProvider {
	return db.NewSpotProvider(connection)
}

func (d Database) SpotComment(connection *db.Connection) *db.SpotCommentProvider {
	return db.NewSpotCommentProvider(connection)
}

func (d Database) SpotCommentReply(connection *db.Connection) *db.SpotCommentReplyProvider {
	return db.NewSpotCommentReplyProvider(connection)
}

func (d Database) SpotImage(connection *db.Connection) *db.SpotImageProvider {
	return db.NewSpotImageProvider(connection)
}
