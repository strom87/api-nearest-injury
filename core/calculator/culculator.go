package calculator

func RatingScore(total int, ratings map[string]int64) float32 {
	value := ratings["1"]*1 + ratings["2"]*2 + ratings["3"]*3 + ratings["4"]*4 + ratings["5"]*5
	return float32(value / int64(total))
}
