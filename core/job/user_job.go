package job

import (
	"errors"
	"sync"

	"bitbucket.org/strom87/api-nearest-injury/core/factory"
	"bitbucket.org/strom87/api-nearest-injury/core/logger"
	"bitbucket.org/strom87/db-nearest-injury"
)

type UserJob struct {
	Logger *logger.Logger
}

func NewUserJob() *UserJob {
	return &UserJob{Logger: logger.NewLogger()}
}

func (u UserJob) UpdateName(user db.User) {
	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		u.Logger.Log("UserJob update name could not open db connection.", err)
		return
	}
	defer conn.Close()

	var wg sync.WaitGroup
	wg.Add(4)

	go u.updateUserNameForSpots(conn, &wg, user)
	go u.updateUserNameForSpotImages(conn, &wg, user)
	go u.updateUserNameForSpotComments(conn, &wg, user)
	go u.updateUserNameForSpotCommentReplies(conn, &wg, user)

	wg.Wait()
}

func (u UserJob) UpdateImage(user db.User, image string) {
	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		u.Logger.Log("UserJob update image could not open db connection.", err)
		return
	}
	defer conn.Close()

	var wg sync.WaitGroup
	wg.Add(4)

	go u.updateUserImageForSpots(conn, &wg, user, image)
	go u.updateUserImageForSpotImages(conn, &wg, user, image)
	go u.updateUserImageForSpotComments(conn, &wg, user, image)
	go u.updateUserImageForSpotCommentReplies(conn, &wg, user, image)

	wg.Wait()
}

func (u UserJob) updateUserNameForSpots(conn *db.Connection, wg *sync.WaitGroup, user db.User) {
	defer wg.Done()

	info, err := factory.NewDatabase().Spot(conn).UpdateAllUserName(user.Id, user.Name)
	if err != nil {
		u.Logger.Log("UserJob updateUserNameForSpots db error.", err)
	}
	if info == nil {
		u.Logger.Log("UserJob updateUserNameForSpots info error.", errors.New("ChangeInfo is nil"))
	}
}

func (u UserJob) updateUserNameForSpotImages(conn *db.Connection, wg *sync.WaitGroup, user db.User) {
	defer wg.Done()

	info, err := factory.NewDatabase().SpotImage(conn).UpdateAllUserName(user.Id, user.Name)
	if err != nil {
		u.Logger.Log("UserJob updateUserNameForSpotImages db error.", err)
	}
	if info == nil {
		u.Logger.Log("UserJob updateUserNameForSpotImages info error.", errors.New("ChangeInfo is nil"))
	}
}

func (u UserJob) updateUserNameForSpotComments(conn *db.Connection, wg *sync.WaitGroup, user db.User) {
	defer wg.Done()

	info, err := factory.NewDatabase().SpotComment(conn).UpdateAllUserName(user.Id, user.Name)
	if err != nil {
		u.Logger.Log("UserJob updateUserNameForSpotComments db error.", err)
	}
	if info == nil {
		u.Logger.Log("UserJob updateUserNameForSpotComments info error.", errors.New("ChangeInfo is nil"))
	}
}

func (u UserJob) updateUserNameForSpotCommentReplies(conn *db.Connection, wg *sync.WaitGroup, user db.User) {
	defer wg.Done()

	info, err := factory.NewDatabase().SpotCommentReply(conn).UpdateAllUserImage(user.Id, user.Name)
	if err != nil {
		u.Logger.Log("UserJob updateUserNameForSpotCommentReplies db error.", err)
	}
	if info == nil {
		u.Logger.Log("UserJob updateUserNameForSpotCommentReplies info error.", errors.New("ChangeInfo is nil"))
	}
}

func (u UserJob) updateUserImageForSpots(conn *db.Connection, wg *sync.WaitGroup, user db.User, image string) {
	defer wg.Done()

	info, err := factory.NewDatabase().Spot(conn).UpdateAllUserImage(user.Id, image)
	if err != nil {
		u.Logger.Log("UserJob updateUserImageForSpots db error.", err)
	}
	if info == nil {
		u.Logger.Log("UserJob updateUserImageForSpots info error.", errors.New("ChangeInfo is nil"))
	}
}

func (u UserJob) updateUserImageForSpotImages(conn *db.Connection, wg *sync.WaitGroup, user db.User, image string) {
	defer wg.Done()

	info, err := factory.NewDatabase().SpotImage(conn).UpdateAllUserImage(user.Id, image)
	if err != nil {
		u.Logger.Log("UserJob updateUserImageForSpotImages db error.", err)
	}
	if info == nil {
		u.Logger.Log("UserJob updateUserImageForSpotImages info error.", errors.New("ChangeInfo is nil"))
	}
}

func (u UserJob) updateUserImageForSpotComments(conn *db.Connection, wg *sync.WaitGroup, user db.User, image string) {
	defer wg.Done()

	info, err := factory.NewDatabase().SpotComment(conn).UpdateAllUserImage(user.Id, image)
	if err != nil {
		u.Logger.Log("UserJob updateUserImageForSpotComments db error.", err)
	}
	if info == nil {
		u.Logger.Log("UserJob updateUserImageForSpotComments info error.", errors.New("ChangeInfo is nil"))
	}
}

func (u UserJob) updateUserImageForSpotCommentReplies(conn *db.Connection, wg *sync.WaitGroup, user db.User, image string) {
	defer wg.Done()

	info, err := factory.NewDatabase().SpotCommentReply(conn).UpdateAllUserImage(user.Id, image)
	if err != nil {
		u.Logger.Log("UserJob setUserImageForSpotCommentReplies db error.", err)
	}
	if info == nil {
		u.Logger.Log("UserJob setUserImageForSpotCommentReplies info error.", errors.New("ChangeInfo is nil"))
	}
}
