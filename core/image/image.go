package image

import (
	"bytes"
	"errors"
	"fmt"
	"hash/crc32"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
	"mime/multipart"
	"os"
	"regexp"
	"strings"
	"time"

	"bitbucket.org/strom87/api-nearest-injury/config"

	"github.com/nfnt/resize"
)

const (
	maxFileSize   int64 = 5000000 // bytes
	minFileSize   int64 = 1       // bytes
	imageBoundary uint  = 1024    // pixels
	imageQuality  int   = 90      // 1 - 100
)

type Image struct {
}

func NewImage() *Image {
	return &Image{}
}

// UploadImages handles uploading of one image
func (i Image) UploadImage(reader *multipart.Reader, name string, folder config.Folder) (string, error) {
	part, err := reader.NextPart()
	if err != nil || part == nil {
		return "", errors.New("No image uploaded")
	}

	img, err := getImageType(part)
	if err != nil {
		return "", err
	}

	img = i.resizeImage(img, imageBoundary)

	if err := i.inFileSizeLimit(img); err != nil {
		return "", err
	}

	fileName := i.generateFileName(name)
	if err := i.saveFileSizes(img, fileName, folder); err != nil {
		return "nil", err
	}

	return fileName, nil
}

// UploadImages handles uploading of multiple images
func (i Image) UploadImages(reader *multipart.Reader, name string, folder config.Folder) ([]string, error) {
	var images []image.Image
	part, err := reader.NextPart()

	for err == nil && part != nil {
		if part.FileName() == "" {
			break
		}

		img, err := getImageType(part)
		if err != nil {
			return nil, err
		}

		img = i.resizeImage(img, imageBoundary)

		if err := i.inFileSizeLimit(img); err != nil {
			return nil, err
		}

		images = append(images, img)
		part, err = reader.NextPart()
	}

	var fileNames []string
	for _, img := range images {
		fileName := i.generateFileName(name)
		if err := i.saveFileSizes(img, fileName, folder); err != nil {
			return nil, err
		}

		fileNames = append(fileNames, fileName)
	}

	return fileNames, nil
}

func (i Image) saveFileSizes(img image.Image, name string, folder config.Folder) error {
	medium := i.resizeImageBoundury(img, 400, 400)
	thumb := i.resizeImageBoundury(img, 200, 200)

	if err := i.save(img, name, folder.Large); err != nil {
		return err
	}

	if err := i.save(medium, name, folder.Medium); err != nil {
		return err
	}

	if err := i.save(thumb, name, folder.Thumb); err != nil {
		return err
	}

	return nil
}

// save saves the file to given directory
func (i Image) save(img image.Image, name, path string) error {
	dest, err := os.Create(path + name)
	if err != nil {
		return err
	}
	defer dest.Close()

	return jpeg.Encode(dest, img, &jpeg.Options{Quality: imageQuality})
}

// resizeImage resizes the image to the given max width or height
func (Image) resizeImage(img image.Image, boundary uint) image.Image {
	// X = width and Y = height
	width := img.Bounds().Size().X
	height := img.Bounds().Size().Y

	if width >= height {
		if width > int(boundary) {
			return resize.Resize(boundary, 0, img, resize.Lanczos3)
		}
	} else if width < height {
		if height > int(boundary) {
			return resize.Resize(0, boundary, img, resize.Lanczos3)
		}
	}

	return img
}

func (Image) resizeImageBoundury(img image.Image, width, height uint) image.Image {
	return resize.Resize(width, height, img, resize.Lanczos3)
}

// inFileSizeLimit checks that the image size is in between the maxFileSize and minFileSize
func (Image) inFileSizeLimit(img image.Image) error {
	var imageBuffer bytes.Buffer
	png.Encode(&imageBuffer, img)

	var buffer bytes.Buffer
	hash := crc32.NewIEEE()
	multiWriter := io.MultiWriter(&buffer, hash)
	limitedReader := &io.LimitedReader{R: &imageBuffer, N: maxFileSize + 1}

	if _, err := io.Copy(multiWriter, limitedReader); err != nil {
		return err
	}

	fileSize := maxFileSize + 1 - limitedReader.N

	if fileSize > maxFileSize {
		return errors.New("File size is to large")
	} else if fileSize < minFileSize {
		return errors.New("File size is to small")
	}

	return nil
}

// getImageType get the image in corresponding encoding type
func getImageType(part *multipart.Part) (image.Image, error) {
	switch part.Header.Get("Content-Type") {
	case "image/jpeg", "image/pjpeg":
		img, err := jpeg.Decode(part)
		if err != nil {
			return nil, err
		}
		return img, nil
	case "image/png":
		img, err := png.Decode(part)
		if err != nil {
			return nil, err
		}
		return img, nil
	case "image/gif":
		img, err := gif.Decode(part)
		if err != nil {
			return nil, err
		}
		return img, nil
	}

	return nil, errors.New("Not an allowed file type")
}

// generateFileName generates a file name with a time stamp of type png
func (i Image) generateFileName(name string) string {
	reg, _ := regexp.Compile("[^A-Za-z0-9]+")
	name = reg.ReplaceAllString(name, "_")
	name = strings.ToLower(strings.Trim(name, "_"))

	return fmt.Sprintf("%s_%s.jpg", name, i.timeStamp())
}

func (Image) dirExist(path string) error {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return errors.New("no such file or directory: " + path)
	}

	return nil
}

// timeStamp generates a time stamp
func (Image) timeStamp() string {
	t := time.Now()
	return fmt.Sprintf("%d%02d%02d_%02d%02d%02d_%04d",
		t.Year(),
		t.Month(),
		t.Day(),
		t.Hour(),
		t.Minute(),
		t.Second(),
		t.Nanosecond()/1000)
}
