package mail

import (
	"bytes"
	"text/template"

	"bitbucket.org/strom87/api-nearest-injury/config"
	"bitbucket.org/strom87/api-nearest-injury/core/encryption"
	"bitbucket.org/strom87/db-nearest-injury"
)

type MailProvider struct {
	*Mail
	Config *config.Config
}

func NewMailProvider() *MailProvider {
	c := config.NewConfig()
	return &MailProvider{Mail: NewMail(c.Mail.Port, c.Mail.SmtpHost, c.Mail.User, c.Mail.Password), Config: c}
}

func (m MailProvider) SendActivationMail(user db.User) error {
	var doc bytes.Buffer
	temp, err := template.ParseFiles("templates/mail/activation.html")
	if err != nil {
		return err
	}

	token, err := encryption.EncryptAes(string(user.Id))
	if err != nil {
		return err
	}

	data := map[string]string{
		"Token": token,
	}

	err = temp.Execute(&doc, data)
	if err != nil {
		return err
	}

	m.Send(m.Config.Mail.EmailAddress.Info, user.Email, "Nearest injury activation", doc.String())
	return nil
}

func (m MailProvider) SendForgottenPasswordMail(user db.User, password string) error {
	var doc bytes.Buffer
	temp, err := template.ParseFiles("templates/mail/forgotten_password.html")
	if err != nil {
		return err
	}

	data := map[string]string{
		"Password": password,
	}

	err = temp.Execute(&doc, data)
	if err != nil {
		return err
	}

	m.Send(m.Config.Mail.EmailAddress.Info, user.Email, "Nearest injury new password", doc.String())
	return nil
}
