package authentication

import (
	"errors"

	"bitbucket.org/strom87/api-nearest-injury/core/encryption"
	"bitbucket.org/strom87/api-nearest-injury/core/factory"
	"bitbucket.org/strom87/api-nearest-injury/core/logger"
	"bitbucket.org/strom87/db-nearest-injury"
)

type Authentication struct {
	Logger *logger.Logger
}

func NewAuthentication() *Authentication {
	return &Authentication{Logger: logger.NewLogger()}
}

func (a Authentication) IsValid(conn *db.Connection, email, password string) (*db.User, error) {
	user, err := factory.NewDatabase().User(conn).FindByEmail(email)
	if err != nil || user == nil {
		return nil, err
	}

	if !encryption.MatchPasswords(password, user.Credential.Password, user.Credential.Salt) {
		return nil, errors.New("Password and hashed password did not match")
	}

	if err := a.RehashPassword(conn, *user, password); err != nil {
		a.Logger.Log("Authentication rehash password error.", err)
	}

	return user, nil
}

func (Authentication) RehashPassword(conn *db.Connection, user db.User, password string) error {
	if !encryption.PasswordRehashNeeded(user.Credential.RehashDate) {
		return nil
	}

	pass, salt := encryption.MakePassword(password)
	user.Credential.Salt = salt
	user.Credential.Password = pass

	if err := factory.NewDatabase().User(conn).Update(user); err != nil {
		return err
	}

	return nil
}
