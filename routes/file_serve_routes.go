package routes

import (
	"bitbucket.org/strom87/api-nearest-injury/controllers"
	"bitbucket.org/strom87/api-nearest-injury/middlewares"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func FileServeRoutes(router *mux.Router, auth *middlewares.Authentication) {
	controller := controllers.NewFileServeController()

	router.Handle("/image/{folder}/{size}/{file}", negroni.New(
		negroni.HandlerFunc(controller.GetImage),
	)).Methods("GET")
}
