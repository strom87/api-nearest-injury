package routes

import (
	"bitbucket.org/strom87/api-nearest-injury/controllers"
	"bitbucket.org/strom87/api-nearest-injury/middlewares"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func AuthRoutes(router *mux.Router, auth *middlewares.Authentication) {
	controller := controllers.NewAuthController()

	router.Handle("/login", negroni.New(
		negroni.HandlerFunc(controller.Login),
	)).Methods("POST")

	router.Handle("/refresh-token", negroni.New(
		negroni.HandlerFunc(controller.Refresh),
	)).Methods("POST")

	router.Handle("/validate-auth-token", negroni.New(
		negroni.HandlerFunc(controller.IsValidAuthToken),
	)).Methods("POST")

	router.Handle("/validate-refresh-token", negroni.New(
		negroni.HandlerFunc(controller.IsValidRefreshToken),
	)).Methods("POST")

	router.Handle("/sign-out", negroni.New(
		auth,
		negroni.HandlerFunc(controller.SignOut),
	)).Methods("POST")
}
