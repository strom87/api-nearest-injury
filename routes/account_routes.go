package routes

import (
	"bitbucket.org/strom87/api-nearest-injury/controllers"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func AccountRoutes(router *mux.Router) {
	controller := controllers.NewAccountController()

	router.Handle("/account/register", negroni.New(
		negroni.HandlerFunc(controller.Register),
	)).Methods("POST")

	router.Handle("/account/activate/{token}", negroni.New(
		negroni.HandlerFunc(controller.Activate),
	)).Methods("GET")

	router.Handle("/account/forgotten-password", negroni.New(
		negroni.HandlerFunc(controller.ForgottenPassword),
	)).Methods("POST")
}
