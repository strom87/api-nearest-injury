package routes

import (
	"bitbucket.org/strom87/api-nearest-injury/middlewares"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func setRoutes(router *mux.Router, auth *middlewares.Authentication) {
	AccountRoutes(router)
	AuthRoutes(router, auth)
	UserRoutes(router, auth)
	SpotRoutes(router, auth)
	MarkerRoutes(router, auth)
	FileServeRoutes(router, auth)
}

func InitRoutes() *negroni.Negroni {
	n := negroni.Classic()
	router := mux.NewRouter()

	n.Use(middlewares.NewJsonHeader())

	setRoutes(router, middlewares.NewAuthentication())

	n.UseHandler(router)
	return n
}
