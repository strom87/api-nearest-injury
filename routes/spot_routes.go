package routes

import (
	"bitbucket.org/strom87/api-nearest-injury/controllers"
	"bitbucket.org/strom87/api-nearest-injury/middlewares"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func SpotRoutes(router *mux.Router, auth *middlewares.Authentication) {
	controller := controllers.NewSpotController()

	router.Handle("/spot/{spotid}", negroni.New(
		auth,
		negroni.HandlerFunc(controller.Get),
	)).Methods("GET")

	router.Handle("/spot", negroni.New(
		auth,
		negroni.HandlerFunc(controller.Create),
	)).Methods("POST")

	router.Handle("/spot/{id}", negroni.New(
		auth,
		negroni.HandlerFunc(controller.Edit),
	)).Methods("PUT")

	router.Handle("/spot/comment", negroni.New(
		auth,
		negroni.HandlerFunc(controller.Comment),
	)).Methods("POST")

	router.Handle("/spot/comment-reply", negroni.New(
		auth,
		negroni.HandlerFunc(controller.CommentReply),
	)).Methods("POST")

	router.Handle("/spot/rate", negroni.New(
		auth,
		negroni.HandlerFunc(controller.Rate),
	)).Methods("POST")

	router.Handle("/spot/like-image", negroni.New(
		auth,
		negroni.HandlerFunc(controller.LikeImage),
	)).Methods("POST")

	router.Handle("/spot/upload-images/{spotid}", negroni.New(
		auth,
		negroni.HandlerFunc(controller.UploadImages),
	)).Methods("POST")
}
