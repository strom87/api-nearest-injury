package routes

import (
	"bitbucket.org/strom87/api-nearest-injury/controllers"
	"bitbucket.org/strom87/api-nearest-injury/middlewares"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func UserRoutes(router *mux.Router, auth *middlewares.Authentication) {
	controller := controllers.NewUserController()

	router.Handle("/user/name", negroni.New(
		auth,
		negroni.HandlerFunc(controller.Name),
	)).Methods("POST")

	router.Handle("/user/profile-image", negroni.New(
		auth,
		negroni.HandlerFunc(controller.ProfileImage),
	)).Methods("POST")
}
