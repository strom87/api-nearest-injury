package routes

import (
	"bitbucket.org/strom87/api-nearest-injury/controllers"
	"bitbucket.org/strom87/api-nearest-injury/middlewares"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func MarkerRoutes(router *mux.Router, auth *middlewares.Authentication) {
	controller := controllers.NewMarkerController()

	router.Handle("/marker/{lat}/{long}/{distance}", negroni.New(
		auth,
		negroni.HandlerFunc(controller.Get),
	)).Methods("GET")

	router.Handle("/marker/{lat}/{long}/{distance}/{userId}", negroni.New(
		//auth,
		negroni.HandlerFunc(controller.GetByPosUser),
	)).Methods("GET")
}
