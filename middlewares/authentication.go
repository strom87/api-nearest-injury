package middlewares

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/strom87/api-nearest-injury/core/factory"
	"bitbucket.org/strom87/api-nearest-injury/core/logger"
	"bitbucket.org/strom87/api-nearest-injury/core/token"
	"bitbucket.org/strom87/api-nearest-injury/models"
	"github.com/gorilla/context"
)

type Authentication struct {
	Logger *logger.Logger
}

func NewAuthentication() *Authentication {
	return &Authentication{Logger: logger.NewLogger()}
}

func (a Authentication) ServeHTTP(w http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		a.Logger.Log("Authentication middleware, could not open db connection.", err)
		a.notAuthenticatedMessage(w)
		return
	}
	defer conn.Close()

	ok, tokenClaim, err := token.NewToken().ValidateAuthToken(req, conn)
	if err != nil {
		a.Logger.Log("Authentication middleware, token validation error.", err)
		a.notAuthenticatedMessage(w)
		return
	} else if !ok {
		a.notAuthenticatedMessage(w)
		return
	}

	context.Set(req, "id", tokenClaim.Id)
	context.Set(req, "platform", tokenClaim.Platform)

	next(w, req)
}

func (a Authentication) notAuthenticatedMessage(w http.ResponseWriter) {
	w.WriteHeader(http.StatusUnauthorized)
	json.NewEncoder(w).Encode(&models.Response{Message: "Unauthorized"})
}
