package middlewares

import "net/http"

type JsonHeader struct{}

func NewJsonHeader() *JsonHeader {
	return &JsonHeader{}
}

func (JsonHeader) ServeHTTP(w http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	w.Header().Set("Content-Type", "application/json;charset=utf8")
	next(w, req)
}
