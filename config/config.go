package config

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

type Config struct {
	Port     string   `json:"port"`
	Rsa      Rsa      `json:"rsa"`
	Mail     Mail     `json:"mail"`
	Token    Token    `json:"token"`
	Database Database `json:"database"`
	Image    Image    `json:"image"`
}

type Database struct {
	Name       string `json:"name"`
	Connection string `json:"connection"`
}

type Mail struct {
	Port         string       `json:"port"`
	SmtpHost     string       `json:"smtp_host"`
	User         string       `json:"user"`
	Password     string       `json:"password"`
	EmailAddress EmailAddress `json:"email_address"`
}

type Token struct {
	AuthExpireTime int `json:"auth_expire_time"`
}

type EmailAddress struct {
	Info string `json:"info"`
}

type Rsa struct {
	PublicFilePath  string `json:"public_file_path"`
	PrivateFilePath string `json:"private_file_path"`
}

type Image struct {
	SpotFolder Folder `json:"spot_folder"`
	UserFolder Folder `json:"user_folder"`
}

type Folder struct {
	Large  string `json:"large"`
	Medium string `json:"medium"`
	Thumb  string `json:"thumb"`
}

func NewConfig() *Config {
	file, err := ioutil.ReadFile("config/config.json")
	if err != nil {
		log.Println("Config NewConfig ReadFile error:", err)
	}

	c := &Config{}
	if err := json.Unmarshal(file, c); err != nil {
		log.Println("Config NewConfig Json Unmarshal error:", err)
	}

	return c
}
