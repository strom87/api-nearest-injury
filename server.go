package main

import (
	"log"
	"net/http"

	"bitbucket.org/strom87/api-nearest-injury/config"
	"bitbucket.org/strom87/api-nearest-injury/routes"
)

func main() {
	port := config.NewConfig().Port
	log.Println("Listening on port:", port)

	negroni := routes.InitRoutes()

	if err := http.ListenAndServe(":"+port, negroni); err != nil {
		log.Fatal(err)
	}
}
