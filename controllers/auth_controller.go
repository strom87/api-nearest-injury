package controllers

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/strom87/api-nearest-injury/core/authentication"
	"bitbucket.org/strom87/api-nearest-injury/core/factory"
	"bitbucket.org/strom87/api-nearest-injury/core/token"
	"bitbucket.org/strom87/api-nearest-injury/models"
	"bitbucket.org/strom87/db-nearest-injury"
	"github.com/strom87/validator-go"
	"gopkg.in/mgo.v2/bson"
)

type AuthController struct {
	*Controller
}

func NewAuthController() *AuthController {
	return &AuthController{NewController()}
}

func (c AuthController) Login(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	model := &models.Login{}
	if err := json.NewDecoder(r.Body).Decode(model); err != nil {
		c.Logger.Log("AuthController login json decode error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid input"})
		return
	}

	if isValid, messages, err := validator.ValidateJson(model); !isValid {
		c.Logger.Log("AuthController login validator error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid input", Errors: messages})
		return
	}

	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		c.Logger.Log("AuthController login could not open db connection.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}
	defer conn.Close()

	user, err := authentication.NewAuthentication().IsValid(conn, model.Email, model.Password)
	if err != nil || user == nil {
		c.Logger.Log("AuthController login authentication is valid error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid email or password"})
		return
	}

	auth, refresh, err := token.NewToken().GetTokens(*user, model.Platform)
	if err != nil || auth == nil || refresh == nil {
		c.Logger.Log("AuthController login get tokens.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not generate token"})
		return
	}

	if !c.setSecret(w, conn, *user, model.Platform, *auth, *refresh) {
		return
	}

	json.NewEncoder(w).Encode(models.Response{
		Data:    models.TokenJson{Token: models.Token{AuthToken: auth.Token, RefreshToken: refresh.Token}},
		Success: true,
	})
}

func (c AuthController) Refresh(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	model := &models.Token{}
	if err := json.NewDecoder(r.Body).Decode(model); err != nil {
		c.Logger.Log("AuthController refresh json decode error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid input"})
		return
	}

	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		c.Logger.Log("AuthController refresh could not open db connection.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}
	defer conn.Close()

	ok, tokenClaim, err := token.NewToken().ValidateRefreshToken(model.RefreshToken, conn)
	if err != nil {
		c.Logger.Log("AuthController refresh validate refresh token error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not validate token"})
		return
	}
	if !ok {
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid refresh token"})
		return
	}

	user, err := factory.NewDatabase().User(conn).Find(bson.ObjectIdHex(tokenClaim.Id))
	if err != nil || user == nil {
		c.Logger.Log("AuthController refresh user porvider find error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not find user"})
		return
	}

	auth, refresh, err := token.NewToken().GetTokens(*user, tokenClaim.Platform)
	if err != nil || auth == nil || refresh == nil {
		c.Logger.Log("AuthController refresh get tokens.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not generate token"})
		return
	}

	if !c.setSecret(w, conn, *user, tokenClaim.Platform, *auth, *refresh) {
		json.NewEncoder(w).Encode(models.Response{Message: "Could not generate token secrets"})
		return
	}

	json.NewEncoder(w).Encode(models.Response{
		Data:    models.TokenJson{Token: models.Token{AuthToken: auth.Token, RefreshToken: refresh.Token}},
		Success: true,
	})
}

func (c AuthController) SignOut(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		c.Logger.Log("AuthController sign out could not open db connection.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}
	defer conn.Close()

	user, err := c.GetUser(r, conn)
	if err != nil || user == nil {
		c.Logger.Log("AuthController sign out get user error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not find user"})
		return
	}

	empty := token.TokenData{}
	if !c.setSecret(w, conn, *user, c.GetPlatform(r), empty, empty) {
		json.NewEncoder(w).Encode(models.Response{Message: "Could not sign out user"})
		return
	}

	json.NewEncoder(w).Encode(models.Response{Message: "User signed out", Success: true})
}

func (c AuthController) IsValidAuthToken(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	model := &models.Token{}
	if err := json.NewDecoder(r.Body).Decode(model); err != nil {
		c.Logger.Log("AuthController is valid auth token json decode error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Not a valid auth token"})
		return
	}

	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		c.Logger.Log("AuthController is valid auth token could not open db connection.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}
	defer conn.Close()

	ok, _, err := token.NewToken().ValidateAuthTokenString(model.AuthToken, conn)
	if err != nil {
		c.Logger.Log("AuthController is valid auth token validate auth token error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Not a valid auth token"})
		return
	}
	if !ok {
		json.NewEncoder(w).Encode(models.Response{Message: "Not a valid auth token"})
		return
	}

	json.NewEncoder(w).Encode(models.Response{Message: "Is valid", Success: true})
}

func (c AuthController) IsValidRefreshToken(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	model := &models.Token{}
	if err := json.NewDecoder(r.Body).Decode(model); err != nil {
		c.Logger.Log("AuthController is valid refresh token json decode error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Not a valid refresh token"})
		return
	}

	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		c.Logger.Log("AuthController is valid refresh token could not open db connection.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}
	defer conn.Close()

	ok, _, err := token.NewToken().ValidateRefreshToken(model.RefreshToken, conn)
	if err != nil {
		c.Logger.Log("AuthController is valid refresh token validate refresh token error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Not a valid refresh token"})
		return
	}
	if !ok {
		json.NewEncoder(w).Encode(models.Response{Message: "Not a valid refresh token"})
		return
	}

	json.NewEncoder(w).Encode(models.Response{Message: "Is valid", Success: true})
}

func (c AuthController) setSecret(w http.ResponseWriter, conn *db.Connection, user db.User, platform string, auth, refresh token.TokenData) bool {
	if platform == token.Web {
		if err := token.NewToken().SetWebTokenSecret(conn, user, auth.Secret, refresh.Secret); err != nil {
			c.Logger.Log("TokenController setSecret set web token secret error.", err)
			return false
		}
	}

	if platform == token.App {
		if err := token.NewToken().SetAppTokenSecret(conn, user, auth.Secret, refresh.Secret); err != nil {
			c.Logger.Log("TokenController setSecret set app token secret error.", err)
			return false
		}
	}

	return true
}
