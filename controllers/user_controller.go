package controllers

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/strom87/api-nearest-injury/config"
	"bitbucket.org/strom87/api-nearest-injury/core/factory"
	"bitbucket.org/strom87/api-nearest-injury/core/image"
	"bitbucket.org/strom87/api-nearest-injury/core/job"
	"bitbucket.org/strom87/api-nearest-injury/models"
	"github.com/strom87/validator-go"
)

type UserController struct {
	*Controller
}

func NewUserController() *UserController {
	return &UserController{NewController()}
}

func (c UserController) Name(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	model := &models.UserName{}
	if err := json.NewDecoder(r.Body).Decode(model); err != nil {
		c.Logger.Log("UserController name json decode error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid input"})
		return
	}

	if isValid, messages, err := validator.ValidateJson(model); !isValid {
		c.Logger.Log("UserController name validator error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid input", Errors: messages})
		return
	}

	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		c.Logger.Log("UserController name could not open db connection.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}
	defer conn.Close()

	user, err := c.GetUser(r, conn)
	if err != nil || user == nil {
		c.Logger.Log("UserController name get user error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not find user"})
		return
	}

	if err := factory.NewDatabase().User(conn).Update(model.ConvertToDb(*user)); err != nil {
		c.Logger.Log("UserController database update user error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not update user"})
		return
	}

	job.NewUserJob().UpdateName(*user)

	json.NewEncoder(w).Encode(models.Response{
		Message: "User name updated",
		Success: true,
	})
}

func (c UserController) ProfileImage(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	reader, err := r.MultipartReader()
	if err != nil {
		c.Logger.Log("UserController profile image read request error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not upload image"})
		return
	}

	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		c.Logger.Log("UserController profile image could not open db connection.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}
	defer conn.Close()

	user, err := c.GetUser(r, conn)
	if err != nil || user == nil {
		c.Logger.Log("UserController profile image get user error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not find user"})
		return
	}

	folder := config.NewConfig().Image.UserFolder
	fileName, err := image.NewImage().UploadImage(reader, user.Name, folder)
	if err != nil {
		c.Logger.Log("UserController profile image upload image error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: err.Error()})
		return
	}

	image := models.NewUserImage().ConvertToDb(*user, fileName)
	if err := factory.NewDatabase().UserImage(conn).Insert(image); err != nil {
		c.Logger.Log("UserController profile image set image error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not save uploaded image"})
		return
	}

	job.NewUserJob().UpdateImage(*user, fileName)

	json.NewEncoder(w).Encode(models.Response{
		Message: "Image saved",
		Success: true,
	})
}
