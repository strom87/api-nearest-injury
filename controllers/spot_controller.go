package controllers

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/strom87/api-nearest-injury/config"
	"bitbucket.org/strom87/api-nearest-injury/core/factory"
	"bitbucket.org/strom87/api-nearest-injury/core/image"
	"bitbucket.org/strom87/api-nearest-injury/models"
	"github.com/strom87/validator-go"
	"gopkg.in/mgo.v2/bson"
)

type SpotController struct {
	*Controller
}

func NewSpotController() *SpotController {
	return &SpotController{NewController()}
}

func (c SpotController) Get(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	spotId, err := c.GetBsonId(r, "spotid")
	if err != nil {
		c.Logger.Log("SpotController get invalid spot id.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid spot id"})
		return
	}

	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		c.Logger.Log("SpotController get could not open db connection.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}
	defer conn.Close()

	spot, err := factory.NewDatabase().Spot(conn).Find(spotId)
	if err != nil || spot == nil {
		c.Logger.Log("SpotController get could not find spot.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not find spot"})
		return
	}

	comments, err := factory.NewDatabase().SpotComment(conn).FindBySpotId(spotId)
	if err != nil || comments == nil {
		c.Logger.Log("SpotController get could not find comments.", err)
	}

	images, err := factory.NewDatabase().SpotImage(conn).FindAllBySpotId(spotId)
	if err != nil || images == nil {
		c.Logger.Log("SpotController get could not find images.", err)
	}

	json.NewEncoder(w).Encode(models.Response{
		Data: models.SpotJson{
			Spot:     models.NewSpot().ConvertFromDb(*spot),
			Images:   models.NewImage().SpotImagesFromDb(images),
			Comments: comments,
		},
		Success: true,
	})
}

func (c SpotController) Create(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	model := &models.Spot{}
	if err := json.NewDecoder(r.Body).Decode(model); err != nil {
		c.Logger.Log("SpotController create json decode error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid input"})
		return
	}

	if isValid, messages, err := validator.ValidateJson(model); !isValid {
		c.Logger.Log("SpotController create validator error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid input", Errors: messages})
		return
	}

	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		c.Logger.Log("SpotController create could not open db connection.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}
	defer conn.Close()

	spots, err := factory.NewDatabase().Spot(conn).FindByGeoLocation(model.Longitude, model.Latitude, 100)
	if err != nil {
		c.Logger.Log("SpotController can not find any location", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}

	if spots != nil {
		json.NewEncoder(w).Encode(models.Response{Message: "Sorry dude! Spot alredy taken!!!"})
		return
	}

	user, err := c.GetUser(r, conn)
	if err != nil || user == nil {
		c.Logger.Log("SpotController create get user error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Can not connect to database"})
		return
	}

	spot := model.ConvertToDb(*user)
	if err := factory.NewDatabase().Spot(conn).Insert(spot); err != nil {
		c.Logger.Log("SpotController get user error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Can not connect to database"})
		return
	}

	json.NewEncoder(w).Encode(models.Response{
		Data:    models.SpotJson{Spot: models.NewSpot().ConvertFromDb(spot)},
		Message: "Spot created",
		Success: true,
	})
}

func (c SpotController) Edit(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	model := &models.SpotEdit{}
	if err := json.NewDecoder(r.Body).Decode(model); err != nil {
		c.Logger.Log("SpotController create json decode error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid input"})
		return
	}

	if isValid, messages, err := validator.ValidateJson(model); !isValid {
		c.Logger.Log("SpotController edit validator error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid input", Errors: messages})
		return
	}

	spotId, err := c.GetBsonId(r, "id")
	if err != nil {
		c.Logger.Log("SpotController edit invalid spot id.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid spot id"})
		return
	}

	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		c.Logger.Log("SpotController edit could not open db connection.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}
	defer conn.Close()

	user, err := c.GetUser(r, conn)
	if err != nil || user == nil {
		c.Logger.Log("SpotController edit get user error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Can not connect to database"})
		return
	}

	spot, err := factory.NewDatabase().Spot(conn).Find(spotId)
	if err != nil || spot == nil {
		c.Logger.Log("SpotController edit find spot error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not find spot"})
		return
	}

	if user.Id != spot.UserId {
		json.NewEncoder(w).Encode(models.Response{Message: "You do not have access to edit this spot"})
		return
	}

	updated := model.Combine(*spot)
	if err := factory.NewDatabase().Spot(conn).Update(updated); err != nil {
		c.Logger.Log("SpotController edit update spot error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not update spot"})
		return
	}

	json.NewEncoder(w).Encode(models.Response{
		Data:    models.SpotJson{Spot: models.NewSpot().ConvertFromDb(updated)},
		Message: "Spot updated",
		Success: true,
	})
}

func (c SpotController) Comment(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	model := &models.Comment{}
	if err := json.NewDecoder(r.Body).Decode(model); err != nil {
		c.Logger.Log("SpotController comment json decode error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid input"})
		return
	}

	if isValid, messages, err := validator.ValidateJson(model); !isValid {
		c.Logger.Log("SpotController comment validator error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid input", Errors: messages})
		return
	}

	if !c.Helper.IsStringObjectId(model.SpotId) {
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid spot id"})
		return
	}

	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		c.Logger.Log("SpotController comment could not open db connection.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}
	defer conn.Close()

	user, err := c.GetUser(r, conn)
	if err != nil || user == nil {
		c.Logger.Log("SpotController comment get user error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not find user"})
		return
	}

	comment := model.ConvertToDb(bson.ObjectIdHex(model.SpotId), *user)
	if err := factory.NewDatabase().SpotComment(conn).Insert(comment); err != nil {
		c.Logger.Log("SpotController comment insert comment error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not create comment"})
		return
	}

	json.NewEncoder(w).Encode(models.Response{
		Data:    models.CommentJson{Comment: comment},
		Message: "Comment created",
		Success: true,
	})
}

func (c SpotController) CommentReply(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	model := &models.CommentReply{}
	if err := json.NewDecoder(r.Body).Decode(model); err != nil {
		c.Logger.Log("SpotController comment reply json decode error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid input"})
		return
	}

	if isValid, messages, err := validator.ValidateJson(model); !isValid {
		c.Logger.Log("SpotController comment reply validator error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid input", Errors: messages})
		return
	}

	if !c.Helper.IsStringObjectId(model.CommentId) {
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid spot id"})
		return
	}

	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		c.Logger.Log("SpotController comment reply could not open db connection.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}
	defer conn.Close()

	user, err := c.GetUser(r, conn)
	if err != nil || user == nil {
		c.Logger.Log("SpotController comment reply get user error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not find user"})
		return
	}

	commentId := bson.ObjectIdHex(model.CommentId)
	reply := model.ConvertToDb(commentId, *user)
	if err := factory.NewDatabase().SpotCommentReply(conn).Insert(reply); err != nil {
		c.Logger.Log("SpotController comment reply insert comment reply error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not create comment"})
		return
	}

	if err := factory.NewDatabase().SpotComment(conn).SetHasReplies(commentId, true); err != nil {
		c.Logger.Log("SpotController comment reply set comment has replies error.", err)
	}

	json.NewEncoder(w).Encode(models.Response{
		Data:    models.CommentReplyJson{CommentReply: reply},
		Message: "Comment reply created",
		Success: true,
	})
}

func (c SpotController) Rate(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	model := &models.Rating{}
	if err := json.NewDecoder(r.Body).Decode(model); err != nil {
		c.Logger.Log("SpotController rate json decode error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid input"})
		return
	}

	if isValid, messages, err := validator.ValidateJson(model); !isValid {
		c.Logger.Log("SpotController rate validator error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid input", Errors: messages})
		return
	}

	if !c.Helper.IsStringObjectId(model.SpotId) {
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid spot id"})
		return
	}

	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		c.Logger.Log("SpotController rate could not open db connection.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}
	defer conn.Close()

	spot, err := factory.NewDatabase().Spot(conn).Find(bson.ObjectIdHex(model.SpotId))
	if err != nil || spot == nil {
		c.Logger.Log("SpotController rate spot provider find error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not find spot"})
		return
	}

	user, err := c.GetUser(r, conn)
	if err != nil || user == nil {
		c.Logger.Log("SpotController rate get user error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not find user"})
		return
	}

	if c.Helper.ContainsBsonId(user.Id, spot.Rating.RatedBy) {
		json.NewEncoder(w).Encode(models.Response{Message: "User have already rated this spot"})
		return
	}

	rating := model.ConvertToDb(spot.Rating, user.Id)
	if err := factory.NewDatabase().Spot(conn).SetRating(spot.Id, rating); err != nil {
		c.Logger.Log("SpotController rate set rating error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Something went wrong, could not rate spot"})
		return
	}

	json.NewEncoder(w).Encode(models.Response{
		Message: "Spot rated",
		Success: true,
	})
}

func (c SpotController) LikeImage(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	model := &models.SpotImageLike{}
	if err := json.NewDecoder(r.Body).Decode(model); err != nil {
		c.Logger.Log("SpotController like image json decode error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid input"})
		return
	}

	if isValid, messages, err := validator.ValidateJson(model); !isValid {
		c.Logger.Log("SpotController like image validator error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid input", Errors: messages})
		return
	}

	if !c.Helper.IsStringObjectId(model.ImageId) {
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid image id"})
		return
	}

	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		c.Logger.Log("SpotController like image could not open db connection.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}
	defer conn.Close()

	user, err := c.GetUser(r, conn)
	if err != nil || user == nil {
		c.Logger.Log("SpotController like get user error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not find user"})
		return
	}

	image, err := factory.NewDatabase().SpotImage(conn).Find(bson.ObjectIdHex(model.ImageId))
	if err != nil || image == nil {
		c.Logger.Log("SpotController like find image error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not find image"})
		return
	}

	if c.Helper.ContainsBsonId(user.Id, image.Like.LikedBy) {
		json.NewEncoder(w).Encode(models.Response{Message: "User have already liked this image"})
		return
	}

	like := model.ConvertToDb(image.Like, user.Id)
	if err := factory.NewDatabase().SpotImage(conn).LikeImage(bson.ObjectIdHex(model.ImageId), like); err != nil {
		c.Logger.Log("SpotController like spot provider like image error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Something went wrong, could not like image"})
		return
	}

	json.NewEncoder(w).Encode(models.Response{
		Message: "Image liked",
		Success: true,
	})
}

func (c SpotController) UploadImages(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	spotId, err := c.GetBsonId(r, "spotid")
	if err != nil {
		c.Logger.Log("SpotController upload images invalid spot id.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid spot id"})
		return
	}

	reader, err := r.MultipartReader()
	if err != nil {
		c.Logger.Log("SpotController upload images body error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not upload image"})
		return
	}

	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		c.Logger.Log("SpotController upload images could not open db connection.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}
	defer conn.Close()

	user, err := c.GetUser(r, conn)
	if err != nil || user == nil {
		c.Logger.Log("SpotController upload images get user.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not find user"})
		return
	}

	spot, err := factory.NewDatabase().Spot(conn).Find(spotId)
	if err != nil || spot == nil {
		c.Logger.Log("SpotController upload images spot provider find error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not find spot"})
		return
	}

	folder := config.NewConfig().Image.SpotFolder
	fileNames, err := image.NewImage().UploadImages(reader, spot.Name, folder)
	if err != nil {
		c.Logger.Log("SpotController upload images upload error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: err.Error()})
		return
	}

	images := models.NewSpotImage().ConvertMultipleToDb(*spot, *user, fileNames)
	if err := factory.NewDatabase().SpotImage(conn).Insert(images); err != nil {
		c.Logger.Log("SpotController upload images push image error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not upload image"})
		return
	}

	json.NewEncoder(w).Encode(models.Response{
		Message: "Images saved",
		Success: true,
	})
}
