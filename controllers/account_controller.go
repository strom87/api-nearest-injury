package controllers

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/strom87/api-nearest-injury/core/encryption"
	"bitbucket.org/strom87/api-nearest-injury/core/factory"
	"bitbucket.org/strom87/api-nearest-injury/core/helper"
	"bitbucket.org/strom87/api-nearest-injury/core/mail"
	"bitbucket.org/strom87/api-nearest-injury/models"
	"github.com/gorilla/mux"
	"github.com/strom87/validator-go"
	"gopkg.in/mgo.v2/bson"
)

type AccountController struct {
	*Controller
}

func NewAccountController() *AccountController {
	return &AccountController{NewController()}
}

func (c Controller) Register(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	model := &models.Register{}
	if err := json.NewDecoder(r.Body).Decode(model); err != nil {
		c.Logger.Log("AccountController register json decode error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid input"})
		return
	}

	if isValid, messages, err := validator.ValidateJson(model); !isValid {
		c.Logger.Log("AccountController register validator error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid input", Errors: messages})
		return
	}

	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		c.Logger.Log("AccountController register could not open db connection.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}
	defer conn.Close()

	exist, err := factory.NewDatabase().User(conn).NameExist(model.Name)
	if err != nil {
		c.Logger.Log("AccountController register user name exist.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}
	if exist {
		json.NewEncoder(w).Encode(models.Response{Message: "User name is already taken"})
		return
	}

	exist, err = factory.NewDatabase().User(conn).EmailExist(model.Email)
	if err != nil {
		c.Logger.Log("AccountController register email name exist.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}
	if exist {
		json.NewEncoder(w).Encode(models.Response{Message: "Email is already registered"})
		return
	}

	if err := factory.NewDatabase().User(conn).Insert(model.ConvertToDb()); err != nil {
		c.Logger.Log("AccountController register insert user error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not register user"})
		return
	}

	user, err := factory.NewDatabase().User(conn).FindByEmail(model.Email)
	if err != nil || user == nil {
		c.Logger.Log("AccountController register could not get user by email.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not send activation mail"})
		return
	}

	if err := mail.NewMailProvider().SendActivationMail(*user); err != nil {
		c.Logger.Log("AccountController register could not send activation mail.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not send activation mail"})
		return
	}

	json.NewEncoder(w).Encode(models.Response{Message: "User registered", Success: true})
}

func (c Controller) Activate(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	vars := mux.Vars(r)
	id, err := encryption.DecryptAes(vars["token"])
	if err != nil {
		c.Logger.Log("AccountController activate invalid token.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid activation token"})
		return
	}

	if ok := c.Helper.IsStringObjectId(id); !ok {
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid activation token type"})
		return
	}

	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		c.Logger.Log("AccountController activate could not open db connection.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}
	defer conn.Close()

	userId := bson.ObjectIdHex(id)

	if exist, err := factory.NewDatabase().User(conn).Exist(userId); err != nil || !exist {
		if err != nil {
			c.Logger.Log("AccountController activate user exist error.", err)
			json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
			return
		}
		if !exist {
			json.NewEncoder(w).Encode(models.Response{Message: "Could not find user"})
			return
		}
	}

	if err := factory.NewDatabase().User(conn).Activate(userId); err != nil {
		c.Logger.Log("AccountController activate activation error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not activate account"})
		return
	}

	json.NewEncoder(w).Encode(models.Response{Message: "Account activated", Success: true})
}

func (c Controller) ForgottenPassword(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	model := &models.ForgottenPassword{}
	if err := json.NewDecoder(r.Body).Decode(model); err != nil {
		c.Logger.Log("AccountController forgotten password json decode error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid input"})
		return
	}

	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		c.Logger.Log("AccountController forgotten password could not open db connection.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}
	defer conn.Close()

	user, err := factory.NewDatabase().User(conn).FindByEmail(model.Email)
	if err != nil || user == nil {
		c.Logger.Log("AccountController forgotten password could not get user by email.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "User not registered"})
		return
	}

	newPassword := helper.NewHelper().RandomString(10)
	password, salt := encryption.MakePassword(newPassword)
	user.Credential.Salt = salt
	user.Credential.Password = password

	if err := factory.NewDatabase().User(conn).Update(*user); err != nil {
		c.Logger.Log("AccountController forgotten password no database connection.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not set new password"})
	}

	if err := mail.NewMailProvider().SendForgottenPasswordMail(*user, password); err != nil {
		c.Logger.Log("AccountController forgotten password could not send new password mail.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not send new password mail"})
		return
	}

	json.NewEncoder(w).Encode(models.Response{Message: "New password sent", Success: true})
}
