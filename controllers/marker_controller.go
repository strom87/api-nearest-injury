package controllers

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/strom87/api-nearest-injury/core/factory"
	"bitbucket.org/strom87/api-nearest-injury/models"
)

type MarkerController struct {
	*Controller
}

func NewMarkerController() *MarkerController {
	return &MarkerController{NewController()}
}

func (c MarkerController) Get(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	long, err := c.GetFloat(r, "long")
	if err != nil {
		c.Logger.Log("NewMarkerController get invalid longitude.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid longitude"})
		return
	}

	lat, err := c.GetFloat(r, "lat")
	if err != nil {
		c.Logger.Log("NewMarkerController get invalid latitude.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid latitude"})
		return
	}

	distance, err := c.GetInt(r, "distance")
	if err != nil {
		c.Logger.Log("NewMarkerController get invalid distance.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid distance"})
		return
	}

	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		c.Logger.Log("MarkerController get could not open db connection.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}
	defer conn.Close()

	spots, err := factory.NewDatabase().Spot(conn).FindByGeoLocation(lat, long, distance)
	if err != nil || spots == nil {
		c.Logger.Log("NewMarkerController get find by geo location error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not find any markers"})
		return
	}

	json.NewEncoder(w).Encode(models.Response{
		Data:    models.MarkerJson{Markers: models.NewMarker().Combine(spots)},
		Success: true,
	})
}

func (c MarkerController) GetByPosUser(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	long, err := c.GetFloat(r, "long")
	if err != nil {
		c.Logger.Log("NewMarkerController get invalid longitude.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid longitude"})
		return
	}

	lat, err := c.GetFloat(r, "lat")
	if err != nil {
		c.Logger.Log("NewMarkerController get invalid latitude.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid latitude"})
		return
	}

	distance, err := c.GetInt(r, "distance")
	if err != nil {
		c.Logger.Log("NewMarkerController get invalid distance.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid distance"})
		return
	}

	userId, err := c.GetBsonId(r, "userId")
	if err != nil {
		c.Logger.Log("NewMarkerController get invalid userId.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Invalid userId"})
		return
	}

	conn := factory.NewDatabase().Connection()
	if err := conn.Open(); err != nil {
		c.Logger.Log("MarkerController get could not open db connection.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not connect to database"})
		return
	}
	defer conn.Close()

	spots, err := factory.NewDatabase().Spot(conn).FindByGeoLocationAndUser(long, lat, distance, userId)
	if err != nil || spots == nil {
		c.Logger.Log("NewMarkerController get find by geo location error.", err)
		json.NewEncoder(w).Encode(models.Response{Message: "Could not find any markers"})
		return
	}

	json.NewEncoder(w).Encode(models.Response{
		Data:    models.MarkerJson{Markers: models.NewMarker().Combine(spots)},
		Success: true,
	})
}
