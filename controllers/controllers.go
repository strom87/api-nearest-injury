package controllers

import (
	"errors"
	"net/http"
	"strconv"

	"bitbucket.org/strom87/api-nearest-injury/core/factory"
	"bitbucket.org/strom87/api-nearest-injury/core/helper"
	"bitbucket.org/strom87/api-nearest-injury/core/logger"
	"bitbucket.org/strom87/db-nearest-injury"
	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

type Controller struct {
	Helper *helper.Helper
	Logger *logger.Logger
}

func NewController() *Controller {
	return &Controller{Helper: helper.NewHelper(), Logger: logger.NewLogger()}
}

// GetUser get the user from context set in auth middleware
func (Controller) GetUser(r *http.Request, conn *db.Connection) (*db.User, error) {
	id := context.Get(r, "id")
	if id == nil {
		return nil, errors.New("No user found")
	}

	user, err := factory.NewDatabase().User(conn).Find(bson.ObjectIdHex(id.(string)))
	if err != nil {
		return nil, err
	}
	if user == nil {
		return nil, errors.New("No user found")
	}

	return user, nil
}

// GetPlatform from current request
func (Controller) GetPlatform(r *http.Request) string {
	return context.Get(r, "platform").(string)
}

// GetBsonId get bson ObjectId from url paramater by id
func (Controller) GetBsonId(r *http.Request, name string) (bson.ObjectId, error) {
	id := mux.Vars(r)[name]
	if !bson.IsObjectIdHex(id) {
		return bson.ObjectId(""), errors.New("Invalid bson object id")
	}

	return bson.ObjectIdHex(id), nil
}

// GetString get int from url paramater by id
func (Controller) GetString(r *http.Request, name string) string {
	return mux.Vars(r)[name]
}

// GetInt get int from url paramater by id
func (Controller) GetInt(r *http.Request, name string) (int64, error) {
	value, err := strconv.ParseInt(mux.Vars(r)[name], 10, 64)
	if err != nil {
		return 0, err
	}

	return value, nil
}

// GetFloat get float from url parameter by id
func (Controller) GetFloat(r *http.Request, name string) (float64, error) {
	value, err := strconv.ParseFloat(mux.Vars(r)[name], 64)
	if err != nil {
		return 0.0, err
	}

	return value, nil
}
