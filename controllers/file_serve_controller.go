package controllers

import (
	"net/http"
	"os"
)

type FileServeController struct {
	*Controller
}

func NewFileServeController() *FileServeController {
	return &FileServeController{NewController()}
}

func (c FileServeController) GetImage(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	path := "./public/images/" + c.GetString(r, "folder") + "/" + c.GetString(r, "size") + "/" + c.GetString(r, "file")
	if f, err := os.Stat(path); err == nil && !f.IsDir() {
		http.ServeFile(w, r, path)
		return
	}

	http.NotFound(w, r)
}
