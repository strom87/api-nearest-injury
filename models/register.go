package models

import (
	"bitbucket.org/strom87/api-nearest-injury/core/encryption"
	"bitbucket.org/strom87/db-nearest-injury"
)

type Register struct {
	Name         string `json:"name"           validator:"required|min:2|max:30"`
	Email        string `json:"email"          validator:"required|email"`
	Password     string `json:"password"       validator:"required|min:6|max:30|equals:ConfPassword"`
	ConfPassword string `json:"conf_password"`
}

func (r Register) ConvertToDb() db.User {
	return db.User{
		Name:       r.Name,
		Email:      r.Email,
		Credential: r.GenerateCredentials(),
	}
}

func (r Register) GenerateCredentials() db.Credential {
	password, salt := encryption.MakePassword(r.Password)

	return db.Credential{
		Salt:       salt,
		Password:   password,
		RehashDate: encryption.GetNewPasswordRehashDate(),
	}
}
