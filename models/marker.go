package models

import (
	"bitbucket.org/strom87/db-nearest-injury"
	"gopkg.in/mgo.v2/bson"
)

type MarkerJson struct {
	Markers []Marker `json:"markers"`
}

type Marker struct {
	SpotId      bson.ObjectId `json:"spot_id"`
	Name        string        `json:"name"`
	Description string        `json:"description"`
	Long        float64       `json:"long"`
	Lat         float64       `json:"lat"`
}

func NewMarker() *Marker {
	return &Marker{}
}

func (Marker) Combine(spots []db.Spot) []Marker {
	var markers []Marker
	for _, v := range spots {
		marker := Marker{
			SpotId:      v.Id,
			Name:        v.Name,
			Description: v.Description,
			Long:        v.Location.Coordinates[1],
			Lat:         v.Location.Coordinates[0],
		}
		markers = append(markers, marker)
	}

	return markers
}
