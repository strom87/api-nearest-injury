package models

import (
	"time"

	"bitbucket.org/strom87/db-nearest-injury"
	"gopkg.in/mgo.v2/bson"
)

type CommentJson struct {
	Comment db.Comment `json:"comment"`
}

type Comment struct {
	SpotId  string `json:"spot_id" validator:"required"`
	Message string `json:"message" validator:"required"`
}

func (c Comment) ConvertToDb(spotId bson.ObjectId, user db.User) db.Comment {
	return db.Comment{
		Id:         bson.NewObjectId(),
		SpotId:     spotId,
		UserId:     user.Id,
		UserName:   user.Name,
		Message:    c.Message,
		HasReplies: false,
		Created:    time.Now(),
	}
}
