package models

import (
	"bitbucket.org/strom87/db-nearest-injury"
	"gopkg.in/mgo.v2/bson"
)

type SpotImageLike struct {
	ImageId string `json:"image_id" validator:"required"`
}

func (SpotImageLike) ConvertToDb(like db.Like, userId bson.ObjectId) db.Like {
	like.Likes += 1
	like.LikedBy = append(like.LikedBy, userId)
	return like
}
