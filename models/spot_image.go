package models

import (
	"bitbucket.org/strom87/db-nearest-injury"
	"gopkg.in/mgo.v2/bson"
)

type SpotImage struct{}

func NewSpotImage() *SpotImage {
	return &SpotImage{}
}

func (SpotImage) ConvertToDb(user db.User, fileName string) db.SpotImage {
	return db.SpotImage{
		Id:       bson.NewObjectId(),
		UserId:   user.Id,
		UserName: user.Name,
		File:     fileName,
	}
}

func (SpotImage) ConvertMultipleToDb(spot db.Spot, user db.User, fileNames []string) []db.SpotImage {
	var images []db.SpotImage
	for _, name := range fileNames {
		image := db.SpotImage{
			Id:       bson.NewObjectId(),
			SpotId:   spot.Id,
			UserId:   user.Id,
			UserName: user.Name,
			File:     name,
		}

		images = append(images, image)
	}

	return images
}
