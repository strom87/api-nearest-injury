package models

import (
	"time"

	"bitbucket.org/strom87/db-nearest-injury"
	"gopkg.in/mgo.v2/bson"
)

type SpotJson struct {
	Spot     SpotCleaned  `json:"spot"`
	Images   []Image      `json:"images"`
	Comments []db.Comment `json:"comments"`
}

type Spot struct {
	Name        string   `json:"name"        validator:"required|max:50"`
	Description string   `json:"description" validator:"required|max:200"`
	Longitude   float64  `json:"longitude"   validator:"required"`
	Latitude    float64  `json:"latitude"    validator:"required"`
	Tags        []string `json:"tags"`
}

type SpotCleaned struct {
	Id          bson.ObjectId `json:"id"`
	UserId      bson.ObjectId `json:"user_id"`
	UserName    string        `json:"user_name"`
	UserImage   string        `json:"user_image"`
	Name        string        `json:"name"`
	Description string        `json:"Description"`
	Created     string        `json:"created"`
	Tags        []string      `json:"tags"`
	Longitude   float64       `json:"longitude"`
	Latitude    float64       `json:"latitude"`
	Rating      float32       `json:"rating"`
}

func NewSpot() *Spot {
	return &Spot{}
}

func (s Spot) ConvertToDb(user db.User) db.Spot {
	return db.Spot{
		Id:          bson.NewObjectId(),
		UserId:      user.Id,
		UserName:    user.Name,
		Name:        s.Name,
		Description: s.Description,
		Tags:        s.Tags,
		Created:     time.Now(),
		Location: db.Location{
			Type:        "Point",
			Coordinates: []float64{s.Longitude, s.Latitude},
		},
		Rating: db.Rating{
			Score:   0,
			RatedBy: []bson.ObjectId{},
			Values: map[string]int64{
				"1": 0,
				"2": 0,
				"3": 0,
				"4": 0,
				"5": 0,
			},
		},
	}
}

func (Spot) ConvertFromDb(spot db.Spot) SpotCleaned {
	return SpotCleaned{
		Id:          spot.Id,
		UserId:      spot.UserId,
		UserName:    spot.UserName,
		UserImage:   spot.UserImage,
		Name:        spot.Name,
		Description: spot.Description,
		Created:     spot.Created.Format("2006-01-02 01:02:03"),
		Tags:        spot.Tags,
		Longitude:   spot.Location.Coordinates[0],
		Latitude:    spot.Location.Coordinates[1],
		Rating:      spot.Rating.Score,
	}
}
