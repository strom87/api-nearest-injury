package models

import "bitbucket.org/strom87/db-nearest-injury"

type UserName struct {
	Name string `json:"name" validator:"required|min:2|max:30"`
}

func (u UserName) ConvertToDb(user db.User) db.User {
	user.Name = u.Name
	return user
}
