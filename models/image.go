package models

import (
	"bitbucket.org/strom87/api-nearest-injury/core/helper"
	"bitbucket.org/strom87/db-nearest-injury"
	"gopkg.in/mgo.v2/bson"
)

type Image struct {
	Id       bson.ObjectId `json:"id"`
	SpotId   bson.ObjectId `json:"spot_id,omitempty"`
	UserId   bson.ObjectId `json:"user_id,omitempty"`
	UserName string        `json:"user_name"`
	Large    string        `json:"large"`
	Medium   string        `json:"medium"`
	Thumb    string        `json:"thumb"`
	Likes    int           `json:"likes"`
}

func NewImage() *Image {
	return &Image{}
}

func (Image) ConvertToDb(user db.User, fileName string) db.Image {
	return db.Image{
		Id:       bson.NewObjectId(),
		UserId:   user.Id,
		UserName: user.Name,
		File:     fileName,
	}
}

func (Image) ConvertMultipleToDb(user db.User, fileNames []string) []db.Image {
	var images []db.Image
	for _, name := range fileNames {
		image := db.Image{
			Id:       bson.NewObjectId(),
			UserId:   user.Id,
			UserName: user.Name,
			File:     name,
		}

		images = append(images, image)
	}

	return images
}

func (i Image) SpotImagesFromDb(images []db.SpotImage) []Image {
	helper := helper.NewHelper()

	var values []Image
	for _, image := range images {
		img := Image{
			Id:       image.Id,
			SpotId:   image.SpotId,
			UserId:   image.UserId,
			UserName: image.UserName,
			Large:    helper.SpotImageUrl("large", image.File),
			Medium:   helper.SpotImageUrl("medium", image.File),
			Thumb:    helper.SpotImageUrl("thumb", image.File),
		}

		values = append(values, img)
	}

	return values
}
