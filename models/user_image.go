package models

import (
	"bitbucket.org/strom87/db-nearest-injury"
	"gopkg.in/mgo.v2/bson"
)

type UserImage struct{}

func NewUserImage() *UserImage {
	return &UserImage{}
}

func (UserImage) ConvertToDb(user db.User, fileName string) []db.UserImage {
	return []db.UserImage{
		db.UserImage{
			Id:       bson.NewObjectId(),
			UserId:   user.Id,
			UserName: user.Name,
			File:     fileName,
		},
	}
}

func (UserImage) ConvertMultipleToDb(user db.User, fileNames []string) []db.UserImage {
	var images []db.UserImage
	for _, name := range fileNames {
		image := db.UserImage{
			Id:       bson.NewObjectId(),
			UserId:   user.Id,
			UserName: user.Name,
			File:     name,
		}

		images = append(images, image)
	}

	return images
}
