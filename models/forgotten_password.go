package models

type ForgottenPassword struct {
	Email string `json:"email" validator:"required|email"`
}
