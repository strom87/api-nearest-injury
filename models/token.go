package models

type TokenJson struct {
	Token Token `json:"token"`
}

type Token struct {
	AuthToken    string `json:"auth_token"`
	RefreshToken string `json:"refresh_token" validator:"required"`
}
