package models

type Login struct {
	Email    string `json:"email" validator:"required|email"`
	Password string `json:"password" validator:"required|min:6|max:30"`
	Platform string `json:"platform" validator:"required"`
}
