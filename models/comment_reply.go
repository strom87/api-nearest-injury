package models

import (
	"time"

	"bitbucket.org/strom87/db-nearest-injury"
	"gopkg.in/mgo.v2/bson"
)

type CommentReplyJson struct {
	CommentReply db.CommentReply `json:"comment_reply"`
}

type CommentReply struct {
	CommentId string `json:"comment_id" validator:"required"`
	Message   string `json:"message"    validator:"required"`
}

func (c CommentReply) ConvertToDb(commentId bson.ObjectId, user db.User) db.CommentReply {
	return db.CommentReply{
		Id:        bson.NewObjectId(),
		CommentId: commentId,
		UserId:    user.Id,
		UserName:  user.Name,
		Message:   c.Message,
		Created:   time.Now(),
	}
}
