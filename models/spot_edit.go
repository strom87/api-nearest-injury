package models

import "bitbucket.org/strom87/db-nearest-injury"

type SpotEdit struct {
	Name        string   `json:"name"        validator:"required|max:50"`
	Description string   `json:"description" validator:"required|max:200"`
	Tags        []string `json:"tags"`
}

func (s SpotEdit) Combine(spot db.Spot) db.Spot {
	spot.Name = s.Name
	spot.Description = s.Description
	spot.Tags = s.Tags

	return spot
}
