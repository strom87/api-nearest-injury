package models

import (
	"bitbucket.org/strom87/api-nearest-injury/core/calculator"
	"bitbucket.org/strom87/db-nearest-injury"
	"gopkg.in/mgo.v2/bson"
)

type Rating struct {
	SpotId string `json:"spot_id" validator:"required"`
	Value  int32  `json:"value"   validator:"required|min:1|max:5"`
}

func (r Rating) ConvertToDb(rating db.Rating, userId bson.ObjectId) db.Rating {
	rating.Values[string(r.Value)] += 1
	rating.RatedBy = append(rating.RatedBy, userId)
	rating.Score = calculator.RatingScore(len(rating.RatedBy), rating.Values)
	return rating
}
