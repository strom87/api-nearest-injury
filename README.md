# Nearest Injury API

```sh
$ go get bitbucket.org/strom87/api-nearest-injury
```

# Curl commands
post file

```sh
curl -i -F filedata=@/path/to/file.jpg http://localhost:1337/
```

post file with authentication

```sh
curl -i -F filedata=@/path/to/file.jpg http://localhost:1337/ -H "Authorization: Bearer {AUTH_TOKEN}"
```

## Routes

~~~go
Authentication routes

    - POST
    Get authentication token
    Auth: Not authenticated
    url: /login
    body: {
        "email": "user email",
        "password": "user password",
        "platform": "web" // web or app
    }

    - POST
    Get refresh token
    Auth: Not authenticated
    url: /refresh-token
    body: {
      "refresh_token": "the refresh token"
    }

    - POST
    Sign out user
    Auth: Need to be authenticated
    url: /sign-out

    - POST
    Validate auth token
    Auth: Not authenticated
    url: /validate-auth-token
    body: {
      "auth_token": "the auth token"
    }

    - POST
    Validate auth token
    Auth: Not authenticated
    url: /validate-refresh-token
    body: {
      "refresh_token": "the refresh token"
    }

Account routes - No routes require user to be authenticated

    - POST
    Register new user
    url: /account/register
    body: {
        "name": "string"
        "email": "string",
        "password": "string",
        "conf_password": "string"
    }

    - GET
    Activate account
    url: /account/activate/{token}

    - POST
    Forgotten password email
    url: /account/forgotten-password
    body: {
        "email": "string"
    }

User routes - All routes require user to be authenticated

    - POST
    Update user name
    url: /user/name
    body: {
        "first_name": "string",
        "last_name": "string"
    }

    - POST
    Upload profile image
    url: /user/profile-image
    body: {
        "first_name": "string",
        "last_name": "string"
    }

Markers routes - All routes require user to be authenticated

    - GET
    Get markers
    url: /marker/{lat}/{long}/{distance}

Spot routes - All routes require user to be authenticated

    - GET
    Get spot by id
    url: /spot/{id}

    - POST
    Create spot
    url: /spot
    body: {
        "name": "string",
        "description": "string",
        "longitude": "float64",
        "latitude": "float64",
        "tags": "string array"
    }

    - PUT
    Edit spot
    url: /spot/{id}
    body: {
        "name": "string",
        "description": "string",
    }

    - POST
    Create comment
    url: /spot/comment
    body: {
        "spot_id": "string",
        "message": "string"
    }

    - POST
    Create comment reply
    url: /spot/comment-reply
    body: {
        "comment_id": "string",
        "message": "string"
    }

    - POST
    Rate spot
    url: /spot/rate
    body: {
        "spot_id": "string",
        "value": "int 1 - 5"
    }

    - POST
    Like image
    url: /spot/like-image
    body: {
        "image_id": "string"
    }

    - POST
    Upload single image
    url: /spot/upload-image/{spotid}
    body: {
        "image": "image"
    }

    - POST
    Upload multiple images
    url: /spot/upload-images/{spotid}
    body: {
        "images": "images"
    }
~~~
